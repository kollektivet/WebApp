import React, {Component} from 'react';
import logo from "../image/logo.PNG";
import {HashRouter as Router, NavLink, Route} from "react-router-dom";
import SignInForm from "./SignInForm";
import SignUpForm from "./SignUpForm";

class LoginPage extends Component {

    static return_login_for_browser() {
        return (
            <Router>
                <div className="App">
                    <div className="App__Aside">
                        <img src={logo} alt="Kollektivet"/>
                        <h1>Kollektivet</h1>
                        <div className="Information_frontPage">
                            <h2 className="Information_frontPage_title">Hva er kollektivet</h2>
                            <p className="Information_FrontPage_Text">
                                Kollektivet er en app for å oss som bor i kollektiv. Med denne appen kan du
                                holde
                                oversikt over alt som må gjøres i ditt kollektiv.
                            </p>
                            <h2 className="Information_frontPage_title">Hva kan jeg gjøre i appen</h2>
                            <p className="Information_FrontPage_Text">
                                I appen kan du invitere de du bor i kollektiv med og opprette oppgaver som må
                                gjøres. Det kan være innkjøp, vasking og betaling av fellesutgifter.
                            </p>
                            <h2 className="Information_frontPage_title">Hvordan fungerer det</h2>
                            <p className="Information_FrontPage_Text">
                                Det er et poengbasert system hvor alle oppgaver gir variert mengde med poeng.
                                Alle oppgaver er åpen for alle å gjøre. En vil dermed få en oversikt over hvor
                                mye
                                hvert medlem av kollektivet har gjort.
                            </p>
                        </div>
                    </div>
                    <div className="App__Form">
                        <div className="PageSwitcher">
                            <NavLink exact to={"/"} activeClassName="PageSwitcher__Item--Active"
                                     className="PageSwitcher__Item">Logg inn</NavLink>
                            <NavLink exact to="/registrer" activeClassName="PageSwitcher__Item--Active"
                                     className="PageSwitcher__Item">Registrer</NavLink>
                        </div>

                        <div className="FormTitle">
                            <NavLink exact to="/" activeClassName="FormTitle__Link--Active"
                                     className="FormTitle__Link">Logg inn</NavLink> eller
                            <NavLink exact to="/registrer" activeClassName="FormTitle__Link--Active"
                                     className="FormTitle__Link">Registrer deg</NavLink>
                        </div>

                        <Route exact path="/" component={SignInForm}>
                        </Route>
                        <Route exact path="/registrer" component={SignUpForm}>
                        </Route>
                    </div>
                </div>
            </Router>
        );
    }

    static return_login_for_mobile() {
        return (
            <Router>
                <div className="App">
                    <div className="App__Form_Mobile">
                        <div className="PageSwitcher_Mobile">
                            <NavLink exact to="/" activeClassName="PageSwitcher__Item--Active"
                                     className="PageSwitcher__Item_mobile">Logg inn</NavLink>
                            <NavLink exact to="/registrer" activeClassName="PageSwitcher__Item--Active"
                                     className="PageSwitcher__Item_mobile">Registrer</NavLink>
                        </div>
                        <img alt="logo" src={logo} className="login__logo"/>

                        <Route exact path="/registrer" component={SignUpForm}>
                        </Route>
                        <Route path="/logginn" component={SignInForm}>
                        </Route>
                        <Route exact path="/" component={SignInForm}>
                        </Route>
                    </div>
                </div>
            </Router>
        )
    }
}

export default LoginPage;