import React from "react";
import CreateTasks from "./CreateTasks";
import KollektivService from "./KollektivService";

export default class MobileNewTask extends React.Component {

    constructor() {
        super();

        this.state = {
            repeatChecked: false,
            taskName: "",
            taskDescription: "",
            taskPoints: "",
            kollektivid: KollektivService.getKollektivID()
        };
        this.handleCheck = this.handleCheck.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.CreateTasks = new CreateTasks();
    }


    handleCheck() {
        this.setState({repeatChecked: !this.state.repeatChecked})
    }

    handleChange(e) {
        let change = {};
        change[e.target.name] = e.target.value;
        this.setState(change)
    }

    handleFormSubmit(e) {

        e.preventDefault();

        this.CreateTasks.createTask(this.state.taskName, this.state.taskDescription, this.state.taskPoints, this.state.kollektivid)
            .then(() => {
                console.log(this.state.kollektivid);
            })
            .catch(error => {
                if (error.toString() === "Error: Not Found") {
                    this.setState({errorMessage: "Feil felt"});
                } else {
                    this.setState({errorMessage: ""})
                }
                console.log(error);
            })
    }

    render() {


        const content = this.state.repeatChecked
            ? <div className="FormField__modal__gjenta__mobile">
                <div className="FormField__modal__gjenta__number__mobile">
                    <label className="FormField__Label__modal">Hvor ofte?</label>
                    <input type="text" id="details" className="FormField__Input" placeholder="Skriv inn et tall"
                           name="taskDescription"/>
                </div>
                <div className="FormField__modal__option__mobile" show={false}>
                    <select className="FormField__select">
                        <option value="years">År</option>
                        <option value="months">Måneder</option>
                        <option selected="selected" value="weeks">Uker</option>
                        <option value="days">Dager</option>
                        <option value="hours">Timer</option>
                    </select>
                </div>
            </div>
            : null;


        return (
            <div className="Mobile__create__task__component">
                <form onSubmit={this.handleFormSubmit} className="FormFields">
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Navn på oppgaven</label>
                        <input type="text" id="taskName" className="FormField__Input" placeholder="Navn på oppgaven"
                               name="taskName" value={this.state.taskName} onChange={this.handleChange}/>
                    </div>

                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Beskrivelse av oppgaven</label>
                        <input type="text" id="taskDescription" className="FormField__Input"
                               placeholder="Skriv en beskrivelse av oppgaven" name="taskDescription"
                               value={this.state.taskDescription}
                               onChange={this.handleChange}/>
                    </div>

                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Hvor mange poeng er oppgaven verdt?</label>
                        <input type="text" id="taskPoints" className="FormField__Input"
                               placeholder="Skriv antall poeng for oppgaven" name="taskPoints"
                               value={this.state.taskPoints}
                               onChange={this.handleChange}/>
                    </div>


                    <div className="FormField__modal__checkbox__mobile">
                        <div className="FormField__modal__checkbox__div__mobile">
                            <label className="FormField__Label__modal__mobile">Gjenta oppgave?</label>
                            <label className="check">
                                <input type="checkbox" checked={this.state.checked} onChange={this.handleCheck}
                                       name="taskRepeat"/>
                                <div className="box"/>
                            </label>
                        </div>
                    </div>

                    <div className="FormField">
                        {content}
                    </div>
                    <div className="FormField">
                        <button className="FormField__Button__mobile mr-20">Legg til oppgave</button>
                    </div>
                </form>
            </div>
        );
    }
}