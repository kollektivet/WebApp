import React, {Component} from "react";
import PieChartComponent from './pieChartComponent';

class MobileStatistics extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div>
                <div>
                    <div className="Mobile__Home__component">
                        <h1 className="Mobile__Home__component__title__section__text">Statistikk</h1>
                        <p>(Kommer snart!)</p>
                        <p>Demo:</p>
                        <div>
                            <PieChartComponent />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MobileStatistics;