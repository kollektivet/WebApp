import AuthService from "./AuthService";
import domain from "./domain";

export default class KollektivService {
    // Initializing important variables

    constructor()
    {
        this.state = {
            liste: [],
            items: []
        };

        KollektivService.fetch = KollektivService.fetch.bind(this);
        KollektivService.getKollektivCode = KollektivService.getKollektivCode.bind(this);
        KollektivService.isMemberInKollektiv = KollektivService.isMemberInKollektiv.bind(this);
        KollektivService.leaveKollektiv = KollektivService.leaveKollektiv.bind(this);
    };


    componentDidMount() {
       KollektivService.returnInvite();
       KollektivService.returnID();

    }

    handleChange(e){
        this.setState({data: e.target.value});
    }

    static isMemberInKollektiv() {

        let data = {
            userid: AuthService.getID()
        };


        fetch(domain.getDomain() + ":8083/kollektiv/isMember/", {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },

            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(isMember => {return isMember})

    }


    static getKollektivCode() {

        let data = {
            userid: AuthService.getID()
        };

        return fetch(domain.getDomain() + ":8083/kollektiv/getInvite/", {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                'Authorization': 'Bearer ' + KollektivService.getCode() + KollektivService.getKollektivID() + KollektivService.getKollektivName()
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.warn(responseData);
                return responseData;

            })
            .then(res => {
                KollektivService.setCode(res.invitekode);
                KollektivService.setKollektivID(res.kollektivid);
                KollektivService.setKollektivName(res.kollektivName);
                return Promise.resolve(res);
            })

    }

    static getTaskList() {

        let data = {
            kollektivid: KollektivService.getKollektivID()
        };


        return fetch(domain.getDomain() + ":8083/kollektiv/getUserTasks/", {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                'Authorization': 'Bearer '

            },

            mode: "cors",
            body: JSON.stringify(data)
        })            .then((response) => response.json())
            .then((responseData) => {
                console.warn(responseData);
                return responseData;

            })
           }

    static leaveKollektiv() {


        let data = {
            userid: AuthService.getID(),
        };
        fetch(domain.getDomain() + ":8083/kollektiv/leaveKollektiv/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .then(console.log(AuthService.getID()))
            .catch(error => console.log(error));

    }



     returnInvite(){
        return this.arr
    }

    static returnID(){
        return this.kollektivid
    }
    static getCode() {
        //Retrieve user id from token
        return localStorage.getItem('invitekode')

    }
    static setCode(invitekode) {
        // Saves user token to localStorage
        localStorage.setItem('invitekode', invitekode)
    }

    static getList() {
        //Retrieve user id from token
        return localStorage.getItem('arr')

    }
    static setList(arr) {
        // Saves user token to localStorage
        localStorage.setItem('arr', arr)
    }

    static getKollektivName() {
        //Retrieve user id from token
        return localStorage.getItem('kollektivName')

    }
    static setKollektivName(kollektivName) {
        // Saves user token to localStorage
        localStorage.setItem('kollektivName', kollektivName)
    }

    static getKollektivID() {
        //Retrieve user id from token
        return localStorage.getItem('kollektivid')

    }
    static setKollektivID(kollektivid) {
        // Saves user token to localStorage
        localStorage.setItem('kollektivid', kollektivid)
    }

    static kollektivCheck() {
        // Checks if there is a saved token and it's still valid
        const invitekode = KollektivService.getCode();
        return !!invitekode
    }
}