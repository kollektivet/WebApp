import React, {Component} from "react";
import {HashRouter as Router, NavLink, Route} from "react-router-dom";
import AuthService from "./AuthService";


export default class MobileMoreMenu extends Component {

    render() {
        return (
            <div className="Mobile__Home__component">
                <Router>
                    <div>
                        <div>
                            <NavLink to="/innstillinger" className="Mobile__Home__component__navlink">
                                <div className="moreMenuItem">
                                    <h1>Innstillinger</h1>
                                </div>
                            </NavLink>
                            <div className="moreMenuItem" onClick={AuthService.logout}>
                                <h1>Logg ut</h1>
                            </div>
                        </div>
                        <Route path="innstillinger"/>
                    </div>
                </Router>
            </div>
        );
    }
}