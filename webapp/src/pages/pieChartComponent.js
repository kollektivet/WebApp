import React, {Component} from 'react';
import {Bar} from 'react-chartjs';


export default class PieChartComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartData:{
                labels:["Frank", "Kristoffer", "Bird person"],
                datasets:[{
                    label:"Poeng",
                    data:[100, 200, 300],
                    backgroundColor:["red", "blue", "yellow"]
                }]

            }

        }
    }

    render() {
        return (
            <div>
                <Bar
                    data={this.state.chartData}
                    options={{
                        maintainAspectRatio: false
                    }}
                />
            </div>
        )
    }
}