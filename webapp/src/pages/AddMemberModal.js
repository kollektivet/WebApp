import React from "react";
import copyImage from "../image/copy.png";
import KollektivService from "./KollektivService";


export default class AddMemberModal extends React.Component {
    constructor() {
        super();

        this.state = {
            repeatChecked: false,
            isCopied: false,
            kollektivCode: KollektivService.getKollektivCode(),
            kollektivKode: KollektivService.getCode(),

        };
        this.copyJoinCode = this.copyJoinCode.bind(this);
    }


    onClose = (e) => {
        this.props.onClose && this.props.onClose(e);
    };

    componentDidMount(){
        }


    copyJoinCode() {
        let copyText = document.getElementById("JoinCode");
        this.setState({isCopied: true});
        copyText.select();
        document.execCommand("copy");
    }





    render() {
        if (!this.props.show) {
            return null;
        }


        const isCopied = this.state.isCopied
            ?  <p>Koden er kopiert!</p>
            : null;





        return (
            <div className="modal">
                {this.props.children}
                <div className="taskFormWrapper">
                    <button className="add__member__close__button" onClick={(e) => {this.onClose(e)}}>X</button>
                    <h1 className="FormField__title">Legg til medlem</h1>
                    <div className="addMemberCodeDiv">
                        <input type="text" id="JoinCode" className="FormField__Input__addMember__modal" value={this.state.kollektivKode} readOnly="readOnly"/>
                        <img onClick={this.copyJoinCode} src={copyImage} alt={copyImage} className="CopyKollektivCodeIcon"/>
                        { isCopied }
                    </div>
                </div>
            </div>
        );
    }
}