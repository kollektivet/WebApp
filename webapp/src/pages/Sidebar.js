import React, {Component} from 'react'
import './style/Sidebar.scss'
import logo from "../image/logo.PNG";
import AuthService from "./AuthService";
import arrowdown from "../image/arrowdown.png";
import Modal from "./AddMemberModal";
import KollektivService from "./KollektivService";
import Settings from "./Desktop/SettingsMenu";

class Sidebar extends Component {



    constructor(props) {
        super(props);
        this.state = {
            showMenu: false,
            showAddMember: false,
            showSettings: false
        };

        this.showMenu = this.showMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
        this.toggleShowSettings = this.toggleShowSettings.bind(this);
    }

    showMenu(event) {
        event.preventDefault();

        this.setState({showMenu: true}, () => {
            document.addEventListener('click', this.closeMenu);
        });
    }

    toggleAddMember = () => {
        this.setState({
            showAddMember: !this.state.showAddMember
        });
    };
    toggleShowSettings = () => {
        this.setState({
            showSettings: !this.state.showSettings
        });
    };


    closeMenu() {
        this.setState({showMenu: false}, () => {
            document.removeEventListener('click', this.closeMenu);
        });
    }


    render() {

        return (
            <div>
            <div className="topBar">
                <div className="logoDiv">
                    <img
                        className="menuIcon"
                        alt="Kollektivet"
                        src={logo}
                        onClick={this.toggleMenu}
                    />
                    <h1 className="kollektivetTitle">Kollektivet</h1>
                </div>
                <div className="menuButtonDivContainer">
                    <div className="menuButtonDiv">
                        <h1 className="menuButtonText" onClick={this.showMenu}>{KollektivService.getKollektivName()}</h1>
                        <div className="arrowDownMenu">
                            <img alt="meny" src={arrowdown} onClick={this.showMenu} className="arrowDownPicture"/>
                        </div>
                        {
                            this.state.showMenu
                                ? (
                                    <div className="menu">
                                        <button className="menuOption" onClick={this.toggleAddMember}> Legg til Medlemmer</button>
                                        <button className="menuOption" onClick={this.toggleShowSettings}> Innstillinger</button>
                                        <button className="menuOption"  onClick={KollektivService.leaveKollektiv}> Forlat Kollektiv</button>
                                        <button className="menuOption" onClick={AuthService.logout}> Logg ut</button>
                                    </div>
                                )
                                : (
                                    null
                                )
                        }
                    </div>
                </div>
            </div>
                <Modal show={this.state.showAddMember} onClose={this.toggleAddMember}/>
                <Settings show={this.state.showSettings} onClose={this.toggleShowSettings}/>
            </div>
        );
    }
}


export default Sidebar