import React, {Component} from "react";
import addButton from "../image/add.png";
import {HashRouter as Router, NavLink, Route} from "react-router-dom";
import MobileNewTask from "./MobileNewTask";
import MobileShowTasks from "./MobileShowTasks";


class MobileTasks extends Component {

    render() {
        return (
            <div>
                <Router>
                    <div>
                        <NavLink to="/oppgaver/ny" className="Mobile__Home__component__navlink">
                            <div className="Mobile__Home__component">
                                <h1 className="Mobile__Home__component__title__section__text">Oppgaver</h1>
                                <img alt="Legg til" src={addButton} className="Mobile__Home__component__title__section__image"/>
                            </div>
                        </NavLink>
                        <Route path="/oppgaver/ny" component={MobileNewTask}/>
                        <Route exact path="/oppgaver" component={MobileShowTasks}/>
                    </div>
                </Router>
            </div>
        );
    }
}

export default MobileTasks;