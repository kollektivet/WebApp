import React, {Component} from "react";
import AuthService from "../AuthService";

export default class SettingsMenu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: AuthService.getName(),

        }
    }

    handleSubmit() {
        console.log("Denne funksjonen eksisterer ikke enda.")
    }


    onClose = (e) => {
        this.props.onClose && this.props.onClose(e);
    };


    render() {

        if (!this.props.show) {
            return null;
        }

        return (
            <div className="Settings__Desktop">
                <div className="taskFormWrapper">
                    <button className="add__member__close__button" onClick={(e) => {
                        this.onClose(e)
                    }}>X
                    </button>
                    <h1>Innstillinger</h1>
                    <p>(Kommer snart)</p>
                    <div className="taskFormDiv">
                        <form onSubmit={this.handleFormSubmit} className="FormFields">
                            <div className="FormField__modal">
                                <label className="FormField__Label__modal" htmlFor="text">Ditt navn</label>
                                <input type="text" id="name" className="FormField__Input__modal"
                                       placeholder="Skal ikke du ha noe navn?"
                                       name="name" value={this.state.name} onChange={this.handleChange}/>
                            </div>

                            <div className="FormField__modal">
                                <label className="FormField__Label__modal" htmlFor="text">Epost</label>
                                <input type="text" id="taskDescription" className="FormField__Input__modal"
                                       placeholder="Skal ikke du ha noe epost?" name="taskDescription"
                                       value={this.state.taskDescription}
                                       onChange={this.handleChange}/>
                            </div>

                            <div className="FormField__modal">
                                <label className="FormField__Label__modal" htmlFor="text">Endre passord</label>
                                <input type="text" id="taskPoints" className="FormField__Input__modal"
                                       placeholder="Skriv inn nytt passord" name="taskPoints"
                                       value={this.state.password}
                                       onChange={this.handleChange}/>
                            </div>
                            <div className="FormField__modal">
                                <label className="FormField__Label__modal" htmlFor="text">Gjenta passord</label>
                                <input type="text" id="repeatPassword" className="FormField__Input__modal"
                                       placeholder="Gjenta passord" name="taskPoints"
                                       value={this.state.repeatPassword}
                                       onChange={this.handleChange}/>
                            </div>
                            <div className="FormField__modal__task__button">
                                <button type="button" onClick={this.handleFormSubmit}
                                        className="FormField__Button mr-20">Lagre
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}