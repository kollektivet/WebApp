import React from "react";
import CreateTasks from "./CreateTasks";
import KollektivService from "./KollektivService";


export default class newTaskModal extends React.Component {
    constructor() {
        super();
        this.state = {
            repeatChecked: false,
            kollektivid: KollektivService.getKollektivID()


        };
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleCheck =this.handleCheck.bind(this);
        this.CreateTasks = new CreateTasks();
    }

    handleChange(e) {
        let change = {};
        change[e.target.name] = e.target.value;
        this.setState(change);
        console.log(this.state.checked)
    }

    handleCheck() {
        this.setState({repeatChecked: !this.state.repeatChecked})
    }

    handleFormSubmit(e) {

        e.preventDefault();

        this.CreateTasks.createTask(this.state.taskName, this.state.taskDescription, this.state.taskPoints, this.state.kollektivid)
            .then(() => {
                console.log(this.state.kollektivid);
            })
            .catch(error => {
                if (error.toString() === "Error: Not Found") {
                    this.setState({errorMessage: "Feil felt"});
                } else {
                    this.setState({errorMessage: ""})
                }
                console.log(error);
            })
    }

    onClose = (e) => {
        this.props.onClose && this.props.onClose(e);
    };

    render() {
        if (!this.props.show) {
            return null;
        }

        const content = this.state.repeatChecked
            ? <div className="FormField__modal__gjenta">
                <div className="FormField__modal__gjenta__number">
                    <label className="FormField__Label__modal">Hvor ofte?</label>
                    <input type="text" id="details" className="FormField__Input__modal" placeholder="Skriv inn et tall"
                           name="taskDescription"/>
                </div>
                <div className="FormField__modal__option" show={false}>
                    <select className="FormField__select">
                        <option value="no_cooldown">Hele tiden (Trenger ikke tall)</option>
                        <option value="years">År</option>
                        <option value="months">Måneder</option>
                        <option selected="selected" value="weeks">Uker</option>
                        <option value="days">Dager</option>
                        <option value="hours">Timer</option>
                    </select>
                </div>
            </div>
            : null;

        return (
            <div className="modal">
                {this.props.children}
                <div className="taskFormWrapper">
                    <button className="add__member__close__button" onClick={(e) => {this.onClose(e)}}>X</button>
                    <h1 className="FormField__title">Legg til en ny oppgave</h1>
                    <form onSubmit={this.handleFormSubmit}>
                        <div className="taskFormDiv">
                            <div className="FormField__modal">
                                <label className="FormField__Label__modal">Navn</label>
                                <input type="text" id="taskName" className="FormField__Input__modal"
                                       value={this.state.taskName} placeholder="Legg inn et oppgavenavn" name="taskName"
                                       onChange={this.handleChange}/>
                            </div>
                            <div className="FormField__modal">
                                <label className="FormField__Label__modal">Detaljer</label>
                                <input type="text" id="details" className="FormField__Input__modal"
                                       value={this.state.taskDescription} placeholder="Legg inn forklaring på oppgaven"
                                       name="taskDescription" onChange={this.handleChange}/>
                            </div>
                            <div className="FormField__modal__number">
                                <label className="FormField__Label__modal">Poeng</label>
                                <input type="number" id="points" className="FormField__Input__modal"
                                       value={this.state.taskPoints} placeholder="Hvor mange poeng er oppgaven verdt?"
                                       name="taskPoints" defaultValue="0" onChange={this.handleChange}/>
                            </div>
                            <div className="FormField__modal__checkbox">
                                <div className="FormField__modal__checkbox__div">
                                    <label className="FormField__Label__modal">Gjenta oppgave?</label>
                                    <label className="check">
                                        <input type="checkbox" checked={this.state.checked} onChange={this.handleCheck}                                               name="taskRepeat"/>
                                        <div className="box"/>
                                    </label>
                                </div>
                                <p className="FormField__text__modal">Du kan velge å gjenta oppgaven om dette er noe som
                                    skal gjøres flere ganger med jamne mellomrom</p>
                            </div>

                            {content}

                            <div className="FormField__modal__task__button">
                                <button type="button" onClick={this.handleFormSubmit}
                                        className="FormField__Button mr-20">Legg til oppgave
                                </button>
                            </div>
                            <div className="FormField__modal__task__button">
                                <button type="button" onClick={(e) => {this.onClose(e)}} className="FormField__Button mr-20">Avbryt
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}