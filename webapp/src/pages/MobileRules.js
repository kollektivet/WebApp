import React, {Component} from "react";
import addButton from "../image/add.png";
import {HashRouter as Router, NavLink, Route} from "react-router-dom";
import MobileNewRule from "./MobileNewRule";


export default class MobileRules extends Component {

    render() {
        return (
            <div>
                <Router>
                    <div>
                        <NavLink to="/regler/ny" className="Mobile__Home__component__navlink">
                            <div className="Mobile__Home__component">
                                <div className="Mobile__Home__component__title__section">
                                    <h1 className="Mobile__Home__component__title__section__text">Regler</h1>
                                    <img alt="Legg til" src={addButton}
                                         className="Mobile__Home__component__title__section__image"/>
                                </div>
                            </div>
                        </NavLink>
                        <Route path="/regler/ny" component={MobileNewRule}/>
                    </div>
                </Router>
            </div>
        );
    }

}