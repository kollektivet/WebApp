import React, {Component} from "react";
import house from "../image/house.png";


export default class joinKollektivComponent extends Component {

    render() {
        return (
            <div className="joinKollektivSide__mobile">
                <h1>Bli med i kollektiv</h1>
                <img src={house} alt="Bli med i eksisterende kollektiv"/>
                <form>
                    <p>Skriv inn invitasjonskode for kollektivet</p>
                    <input name="code" placeholder="Skriv inn kode"/>
                    <br/>
                        <button className="FormField__Button__join__kollektiv__mobile">Bli med!</button>
                </form>
            </div>
        );
    }
}