import React, {Component} from "react";

export default class MobileNewRule extends Component {

    constructor() {
        super();

        this.state = { repeatChecked: false};
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange() {
        this.setState({ repeatChecked: !this.state.repeatChecked})
    }




    render() {
        return (
            <div className="Mobile__create__task__component">
                <form onSubmit={""} className="FormFields">
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Navn på Regelen</label>
                        <input type="text" id="taskName" className="FormField__Input" placeholder="Navn på relegen"
                               name="ruleName" value={this.state.ruleName} onChange={this.handleChange}/>
                    </div>

                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Beskrivelse av regelen</label>
                        <input type="text" id="ruleDescription" className="FormField__Input"
                               placeholder="Skriv en beskrivelse av regelen" name="taskDescription" value={this.state.ruleDescription}
                               onChange={this.handleChange}/>
                    </div>

                    <div className="FormField">
                        <button className="FormField__Button__mobile mr-20">Legg til regel</button>
                    </div>
                </form>
            </div>
        );
    }
}