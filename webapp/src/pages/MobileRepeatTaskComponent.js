import React, {Component} from "react";

export default class MobileRepeatTaskComponent extends Component{


    render() {
        return (
            <div className="FormField__modal__gjenta__mobile">
                <div className="FormField__modal__gjenta__number__mobile">
                    <label className="FormField__Label__modal">Hvor ofte?</label>
                    <input type="text" id="details" className="FormField__Input" placeholder="Skriv inn et tall" name="taskDescription"/>
                </div>
                <div className="FormField__modal__option__mobile" show={false}>
                    <select className="FormField__select">
                        <option value="years">År</option>
                        <option value="months">Måneder</option>
                        <option selected="selected" value="weeks">Uker</option>
                        <option value="days">Dager</option>
                        <option value="hours">Timer</option>
                    </select>
                </div>
            </div>
        );
    }
}