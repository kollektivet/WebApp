import React from "react";
import AuthService from "../AuthService";

export default class SettingsMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: AuthService.getName(),


        }
    }

    render() {
        return (
            <div className="Settings__Mobile">
                <h1>Innstillinger</h1>
                <p>(Kommer snart)</p>
                <form onSubmit={this.handleFormSubmit} className="FormFields">
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Ditt navn</label>
                        <input type="text" id="name" className="FormField__Input" placeholder="Skal ikke du ha noe navn?"
                               name="name" value={this.state.name} onChange={this.handleChange}/>
                    </div>

                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Epost</label>
                        <input type="text" id="taskDescription" className="FormField__Input"
                               placeholder="Skal ikke du ha noe epost?" name="taskDescription"
                               value={this.state.taskDescription}
                               onChange={this.handleChange}/>
                    </div>

                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Endre passord</label>
                        <input type="text" id="taskPoints" className="FormField__Input"
                               placeholder="Skriv inn nytt passord" name="taskPoints"
                               value={this.state.password}
                               onChange={this.handleChange}/>
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Gjenta passord</label>
                        <input type="text" id="repeatPassword" className="FormField__Input"
                               placeholder="Gjenta passord" name="taskPoints"
                               value={this.state.repeatPassword}
                               onChange={this.handleChange}/>
                    </div>
                    <div className="FormField">
                        <button className="FormField__Button__mobile mr-20">Lagre</button>
                    </div>
                </form>
            </div>
        );
    }

}