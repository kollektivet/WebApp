import React, {Component} from "react";

export default class MobileShowTasks extends Component {

    render() {
        return (
            <div className="Mobile__show__tasks__component">
                <p>This is where we find the task list</p>
            </div>
        );
    }
}