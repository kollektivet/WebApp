import React, {Component} from 'react';
import {HashRouter as Router, Route} from "react-router-dom";
import {NavLink} from "react-router-dom";
import CreateTasks from "./CreateTasks";
import ShowTask from "./ShowTask";
import ShowKollektiv from "./ShowKollektiv";
import CreateKollektiv from "./CreateKollektiv";
import Sidebar from "./Sidebar";
import addIcon from "../image/add.png";
import Modal from "./newTaskModal";
import {isBrowser, isMobile} from "react-device-detect";
import checklist from "../image/checklist.png";
import graph from "../image/graph.png";
import hammer from "../image/hammer2.png";
import moreIcon from "../image/moreicon.png";
import MobileTasks from "./MobileTasks";
import MobileStatistics from "./MobileStatistics";
import MobileRules from "./MobileRules";
import KollektivService from "./KollektivService";
import MobileMoreMenu from "./MobileMoreMenu";
import PieChartComponent from './pieChartComponent';
import SettingsMenu from "./Mobile/SettingsMenu"


class HomePage extends Component {

    state = {
        list: KollektivService.getTaskList.list,
        show: false
    };

    componentDidMount() {
        this._postData();
        this._deleteData();
        this.getDoneTasks();
        //this.setState({task: this._postData})

    }

    _postData = () => {

        let data = {
            kollektivid: KollektivService.getKollektivID()
        };

        fetch("http://localhost:8083/kollektiv/getUserTasks/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: "cors",
            body: JSON.stringify(data)

        })
            .then(response => response.json())
            .then(data => this.setState({data: data}))
            .catch(error => console.log(error));
    };


    _deleteData = () => {

        let data = {
            taskid: ''
        };

        fetch("http://localhost:8083/kollektiv/deleteTask/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: "cors",
            body: JSON.stringify(data)

        })
            .then(response => response.json())
            .then(data => this.setState({data:data}))
            .catch(error => console.log(error));
    };

    getDoneTasks = () => {

        let data = {
            kollektivid: KollektivService.getKollektivID()
        };

        fetch("http://localhost:8083/kollektiv/getUserTasksDone/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: "cors",
            body: JSON.stringify(data)

        })
            .then(response => response.json())
            .then(data => this.setState({done:data}))
            .catch(error => console.log(error));
    };

    showModal = () => {
        this.setState({
            show: !this.state.show
        });
    };

    render() {
        if (isBrowser) {
            return (
                <div>
                    <Sidebar/>
                    <div className="home">
                        <div className="homeContent">
                            <div>
                                <h1 className="homeContentTitle">Oppgaver</h1>
                                <img
                                    className="addIcon"
                                    alt="Legg til ny oppgave"
                                    src={addIcon}
                                    onClick={this.showAddTaskModal}
                                />
                            </div><h3>Tilgjenglige Oppgaver: </h3>
                                <div className="nameList">
                                    {
                                        this.state.data &&
                                        this.state.data.map( (item, key) =>
                                            <div key={key}>
                                                {item}
                                            </div>
                                        )}
                                </div>
                           <h3>Ferdige Oppgaver: </h3>
                            <div className="nameList">

                                {
                                    this.state.done &&
                                    this.state.done.map( (item, key) =>
                                        <div key={key}>
                                            {item}
                                        </div>
                                    )}
                            </div>
                        </div>
                        <Modal show={this.state.show} onClose={this.showAddTaskModal}>
                        </Modal>
                        <div className="homeContent">
                            <h1 className="homeContentTitle">Statistikk</h1>
                            <PieChartComponent/>
                        </div>

                        <div className="homeContent">
                            <h1 className="homeContentTitle">Regler</h1>
                            <img
                                className="addIcon"
                                alt="Legg til ny regel"
                                src={addIcon}
                                onClick={CreateTasks}
                            />
                        </div>
                    </div>

                    <Router>
                        <div>
                            <Route path="/task" component={CreateTasks}>
                            </Route>
                            <Route path="/task2" component={ShowTask}>
                            </Route>
                            <Route path="/kollektiv" component={ShowKollektiv}>
                            </Route>
                            <Route path="/nyttkollektiv" component={CreateKollektiv}>
                            </Route>
                        </div>
                    </Router>
                </div>
            );
        } else if (isMobile) {
            return (
                <Router>
                    <div>
                        <div className="PageSwitcher__home_Mobile">
                            <NavLink to="/oppgaver"
                                     activeClassName="PageSwitcher__home__Item_mobile__Item--Active"
                                     className="PageSwitcher__home__Item_mobile"><img alt="Oppgaver" src={checklist}
                                                                                      className="checklistImage"/>
                            </NavLink>
                            <NavLink to="/statistikk"
                                     activeClassName="PageSwitcher__home__Item_mobile__Item--Active"
                                     className="PageSwitcher__home__Item_mobile">
                                <img alt="Statestikk" src={graph} className="checklistImage"/>
                            </NavLink>

                            <NavLink to="/regler"
                                     activeClassName="PageSwitcher__home__Item_mobile__Item--Active"
                                     className="PageSwitcher__home__Item_mobile">
                                <img alt="Regler" src={hammer} className="checklistImage"/>
                            </NavLink>

                            <NavLink to="/meny"
                                     activeClassName="PageSwitcher__home__Item_mobile__Item--Active"
                                     className="PageSwitcher__home__Item_mobile">
                                <img alt="Mer" src={moreIcon} className="checklistImage"/>
                            </NavLink>

                        </div>
                        <Route path="/oppgaver" component={MobileTasks}/>
                        <Route path="/statistikk" component={MobileStatistics}/>
                        <Route path="/regler" component={MobileRules}/>
                        <Route path="/meny" component={MobileMoreMenu}/>
                        <Route path="/innstillinger" component={SettingsMenu}/>
                    </div>
                </Router>
            )
        }
    }
}

export default HomePage;
