import decode from 'jwt-decode';
import domain from "./domain";
export default class AuthService {
    // Initializing important variables
    constructor() {
        this.domain = domain.getDomain() + ":8082";
        AuthService.fetch = AuthService.fetch.bind(this); // React binding stuff
        this.login = this.login.bind(this);
        AuthService.getProfile = AuthService.getProfile.bind(this)
    }

    login(email, password) {
        // Get a token from api server using the fetch api
        return AuthService.fetch(`${this.domain}/user/login/`, {
            method: 'POST',
            body: JSON.stringify({
                email,
                password
            })
        }).then(res => {
            AuthService.setToken(res.token); // Setting the token in localStorage
            AuthService.setID(res.id);
            AuthService.setName(res.name);
            AuthService.setEmail(res.email);
            AuthService.setTaskList(res.liste);
            return Promise.resolve(res);
        })
    }

    static loggedIn() {
        // Checks if there is a saved token and it's still valid
        const token = AuthService.getToken(); // GEtting token from localstorage
        return !!token && !AuthService.isTokenExpired(token) // handwaiving here
    }

    static isTokenExpired(token) {
        try {
            const decoded = decode(token);
            return decoded.exp < Date.now() / 1000;
        }
        catch (err) {
            return false;
        }
    }

    static setToken(idToken) {
        // Saves user token to localStorage
        localStorage.setItem('id_token', idToken)
    }

    static getToken() {
        // Retrieves the user token from localStorage
        return localStorage.getItem('id_token')
    }

    static setEmail(email) {
        // Saves user token to localStorage
        localStorage.setItem('email', email)
    }

    static getEmail() {
        // Retrieves the user token from localStorage
        return localStorage.getItem('email')
    }

    static getName() {
        //Retrieve user id from token
        return localStorage.getItem('name')
    }

    static getID() {
        //Retrieve user id from token
        return localStorage.getItem('id')

    }
    static setName(name) {
        // Saves user token to localStorage
        localStorage.setItem('name', name)
    }

    static getTaskList(){
        return localStorage.getItem('liste')
    }

    static setTaskList(liste) {
        // Saves user token to localStorage
        localStorage.setItem('liste', liste)
    }

    static setID(userID) {
        // Saves user token to localStorage
        localStorage.setItem('id', userID)
    }



    static logout() {
        // Clear user token and profile data from localStorage
        localStorage.removeItem('id_token');
        window.location.href = '/';
    }

    static getProfile() {
        // Using jwt-decode npm package to decode the token
        return decode(AuthService.getToken());
    }


    static fetch(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        // Setting Authorization header
        // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
        if (AuthService.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + AuthService.getToken() + AuthService.getID() + AuthService.getTaskList() + AuthService.getName() + AuthService.getEmail()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(AuthService._checkStatus)
            .then(response => response.json())
    }

    static _checkStatus(response) {
        // raises an error in case response status is not a success
        if (response.status >= 200 && response.status < 300) { // Success status lies between 200 to 300
            return response
        } else {
            var error = new Error(response.statusText);
            error.response = response;
            throw error
        }
    }

}