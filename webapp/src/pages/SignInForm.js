import React, {Component} from 'react';
import AuthService from "./AuthService";

class SignInForm extends Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            errorMessage: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.AuthService = new AuthService();
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleFormSubmit(e){
        e.preventDefault();

        this.AuthService.login(this.state.email,this.state.password)
            .then(() =>{
                window.location.reload();
            })
            .catch(error =>{
                if(error.toString() === "Error: Not Found") {
                    this.setState( { errorMessage: "Epost eller passord er feil" } );
                }
                else {
                    this.setState({errorMessage: ""})
                }
                console.log(error);
            })
    }

    refresh() {
        window.location.reload();
    }

    componentWillMount(){
        if(AuthService.loggedIn())
            this.props.history.replace('/home');
    }


    render() {
        return (
            <div className="FormCenter">
                <form className="FormFields" onSubmit={this.handleFormSubmit}>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="email">Epost adresse</label>
                        <input type="email" id="email" className="FormField__Input" placeholder="Skriv inn epost adresse" name="email" value={this.state.email} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="password">Passord</label>
                        <input type="password" id="password" className="FormField__Input" placeholder="Skriv inn passord" name="password" value={this.state.password} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        {this.state.errorMessage}
                        <br/>
                        <button className="FormField__Button mr-20">Logg inn</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default SignInForm;