import React, {Component} from "react";
import hammer from "../image/hammer.png";
import AuthService from "./AuthService";

class createKollektivComponent extends Component {
    constructor() {
        super();
        this.state = {
            userid: '',
            data: [],

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.createKollektiv = this.createKollektiv.bind(this)

    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        console.log('The form was submitted with the following data:');
        console.log(this.state);
    }


    createKollektiv(event) {
        event.preventDefault();

        let data = {
            kollektivName: this.state.kollektivName,
            userid: AuthService.getID()

        };
        console.log('Kollektiv navn', this.state.kollektivName, 'brukerid ', this.state.userid,);
        fetch("https://kollektivet.app:8083/kollektiv/createKollektiv/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
        this.setState({message: "Du har nå laget et nytt kollektiv!"})

    }

    render() {
        return (
                <div className="createNewKollektivSide__mobile">
                    <h1>Lag et nytt kollektiv</h1>
                    <img src={hammer} alt="Lag et nytt kollektiv"/>
                    <form onSubmit={this.createKollektiv}>
                        <p>Skriv inn navn på ditt nye kollektiv</p>
                        <input name="name" placeholder="Skriv inn navn" value={this.state.name} onChange={this.handleChange}/>
                        <br/>
                        <button className="FormField__Button__create__kollektiv__mobile">Lag nytt!</button>
                    </form>
                </div>
        );
    }
}

export default createKollektivComponent;