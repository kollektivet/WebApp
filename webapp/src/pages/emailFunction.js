import {Component} from "react";

class emailFunction extends Component {
    contructor() {

        this.send_email = this.send_email.bind(this);
        this.send_register_email = this.send_register_email.bind(this);

    }


    send_email(subject, message, sender, recipient) {

        let data={

            subject: subject,
            message: message,
            sender: sender,
            recipient: recipient
        };

        fetch("https://kollektivet.app:8081/email/send", {

            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
    }

    send_register_email(recipient, activationcode) {

        let data ={
            recipient: recipient,
            activationcode: activationcode
        };

        fetch("https://kollektivet.app:8081/email/register", {

            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
    }


    send_reset_password_(resetpasswordcode, recipient) {

        let data ={
            recipient: recipient,
            resepasswordcode: resetpasswordcode
        };

        fetch("https://kollektivet.app:8081/email/resetpassord", {

            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
    }


    send_to_all_users(subject, message, sender) {

        let data={
            subject: subject,
            message: message,
            sender: sender
        };

        fetch("https://kollektivet.app:8081/email/sendtoall", {

            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
    }

    send_newsletter(subject, message, sender) {

        let data={
            subject: subject,
            message: message,
            sender: sender
        };

        fetch("https://kollektivet.app:8081/email/newsletter", {

            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
    }


    send_password_has_changed_email(recipient) {

        let data ={
            recipient: recipient,
        };

        fetch("https://kollektivet.app:8081/email/passwordchanged", {

            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
    }


    send_email_has_changed_email(recipient) {

        let data ={
            recipient: recipient,
        };

        fetch("https://kollektivet.app:8081/email/emailchanged", {

            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
    }

}

export default emailFunction;

