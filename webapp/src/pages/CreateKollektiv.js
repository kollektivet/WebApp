import React, { Component } from 'react';
import AuthService from "./AuthService";


class CreateKollektiv extends Component {
    constructor() {
        super();
        this.AuthService = new AuthService();
        this.state = {
            userid: '',
            data: [],


        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.createKollektiv = this.createKollektiv.bind(this)

    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        console.log('The form was submitted with the following data:');
        console.log(this.state);
    }


    createKollektiv(event) {
        event.preventDefault();

        let data = {
            kollektivName: this.state.kollektivName,
            userid: AuthService.getID()

        };
            console.log('Kollektiv navn', this.state.kollektivName, 'brukerid ', this.state.userid,);
            fetch("https://kollektivet.app:8083/kollektiv/createKollektiv/", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                mode: "cors",
                body: JSON.stringify(data)
            })
                .then(response => response.json())
                .then(data => console.log(data))
                .catch(error => console.log(error));
            this.setState({message: "Du har nå laget et nytt kollektiv!"})

        }


    render() {
        return (
            <div className="FormCenter">
                <div> test {}</div>
                <form onSubmit={this.createKollektiv} className="FormFields">
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="text">Navn på Kollektiv</label>
                        <input type="text" id="kollektivName" className="FormField__Input" placeholder="Navn på kollektiv" name="kollektivName" value={this.state.kollektivName} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <button className="FormField__Button mr-20">Lag Kollektiv</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default CreateKollektiv;