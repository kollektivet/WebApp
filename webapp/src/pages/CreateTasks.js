import decode from 'jwt-decode';

export default class CreateTasks {
    // Initializing important variables
    constructor() {
        this.domain = "http://localhost:8083"; // API server domain
        CreateTasks.fetch = CreateTasks.fetch.bind(this); // React binding stuff
        this.createTask = this.createTask.bind(this);

    }

    createTask(taskName, taskDescription, taskPoints, kollektivid) {
        // Get a token from api server using the fetch api
        return CreateTasks.fetch(`${this.domain}/kollektiv/createTask/`, {
            method: 'POST',
            mode: "cors",
            body: JSON.stringify({
                taskName,
                taskDescription,
                taskPoints,
                kollektivid
            })
        }).then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
    }



    static getProfile() {
        // Using jwt-decode npm package to decode the token
        return decode(CreateTasks.getToken());
    }


    static fetch(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        return fetch(url, {
            headers,
            ...options
        })
            .then(CreateTasks._checkStatus)
            .then(response => response.json())
    }

    static _checkStatus(response) {
        // raises an error in case response status is not a success
        if (response.status >= 200 && response.status < 300) { // Success status lies between 200 to 300
            return response
        } else {
            let error = new Error(response.statusText);
            error.response = response;
            throw error
        }
    }

}