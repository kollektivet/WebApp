
import React, { Component } from 'react';
import AuthService from "./AuthService";



class ShowKollektiv extends Component {

    constructor(props) {
        super(props);
        this.AuthService = new AuthService();
        this.state = {
            userid: AuthService.getID(),
        };
    }


    componentDidMount() {
        this._postData();
       // this.setState({task: this._postData})

    }






    _postData = () => {

        let data = {
            userid: this.state.userid,
        };

        fetch("https://kollektivet.app/kollektiv/showKollektiv/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: "cors",
            body: JSON.stringify(data)

        })
            .then(response => response.json())
            .then(data => this.setState({data:data}))
            .catch(error => console.log(error));

    };




    render() {

        return (

            <div className="nameList">
                {
                    this.state.data &&
                    this.state.data.map( (item, key) =>
                        <div key={key}>
                            {item}
                        </div>

                    )}
            </div>
        )}
}

export default ShowKollektiv;







