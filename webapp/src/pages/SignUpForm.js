import React, {Component} from 'react';
import domain from "./domain";

class SignUpForm extends Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            name: '',
            hasAgreed: false,
            repeatPassword: '',
            message: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.createUser = this.createUser.bind(this);
        this.checkPasswords = this.checkPasswords.bind(this);
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
            [name]: value
        });
    }

    checkPasswords() {

        if (this.state.password === this.state.repeatPassword) {
            this.setState( { message: "" } );
            return true;
        }
        else {
            this.setState( { message: "Passordene er ikke like" } );
            return false;
        }
    }

    createUser(event) {
        event.preventDefault();

        let data = {
            name: this.state.name,
            password: this.state.password,
            repeatPassword: this.state.repeatPassword,
            email: this.state.email
        };

        if (this.state.name !== '' && this.state.password !== '' && this.state.email !== '' && this.checkPasswords()) {
            console.log('name', this.state.name, 'password ', this.state.password, 'email ', this.state.email);
                fetch(domain.getDomain() + ":8082/user/register/", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    mode: "cors",
                    body: JSON.stringify(data)
                })
                    .then(response => response.json())
                    .then(data => console.log(data))
                    .catch(error => console.log(error));
            this.setState({message: "Du er nå registrert! For å aktivere din konto trykk på linken som vi har sendt til deg på epost"});
            this.setState({name: ""});
            this.setState({password: ""});
            this.setState({repeatPassword: ""});
            this.setState({email: ""});
        }
    }


    render() {
        return (
            <div className="FormCenter">
                <form onSubmit={this.createUser} className="FormFields">
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="name">Navn</label>
                        <input type="text" id="username" className="FormField__Input" placeholder="Skriv inn navnet ditt" name="name" value={this.state.name} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="password">Passord</label>
                        <input type="password" id="password" className="FormField__Input" placeholder="Skriv inn passord" name="password" value={this.state.password} onKeyUp={this.checkPasswords} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="password">Gjenta passord</label>
                        <input type="password" id="password" className="FormField__Input" placeholder="Skriv inn passord igjen" name="repeatPassword" value={this.state.repeatPassword} onKeyUp={this.checkPasswords} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="email">Epost adresse</label>
                        <input type="email" id="email" className="FormField__Input" placeholder="Skriv inn epost adresse" name="email" value={this.state.email}  onChange={this.handleChange} />
                    </div>

                    <div className="FormField">
                        <label className="FormField__CheckboxLabel">
                        </label>
                    </div>
                    <div className="FormField">
                        <p id="message">{this.state.message}</p>
                    </div>
                    <div className="FormField">
                        <button className="FormField__Button mr-20">Registrer</button>
                    </div>
                </form>
            </div>
        );
    }
}
export default SignUpForm;