import React, {Component} from 'react';
import {isBrowser, isMobile} from "react-device-detect";
import {NavLink, Route} from "react-router-dom";
import hammer from "../image/hammer.png";
import house from "../image/house.png";
import joinKollektivComponent from "./joinKollektivComponent";
import createKollektivComponent from "./createKollektivComponent";
import AuthService from "./AuthService";
import domain from "./domain";


class notInKollektivPage extends Component {

    constructor() {
        super();
        this.state = {
            userid: '',
            kollektivName: "",
            code: "",

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.createKollektiv = this.createKollektiv.bind(this)
        this.JoinKollektiv = this.JoinKollektiv.bind(this)

    }

    handleChange(e) {
        this.setState({kollektivName: e.target.value});
        this.setState({code: e.target.value});
        console.log("kollektivnavn: " + this.state.kollektivName);

    }

    handleSubmit(e) {
        e.preventDefault();

        console.log('The form was submitted with the following data:');
        console.log(this.state);
    }

    createKollektiv(event) {
        event.preventDefault();

        let data = {
            kollektivName: this.state.kollektivName,
            userid: AuthService.getID()
        };
        console.log('Kollektiv navn', this.state.kollektivName, 'brukerid ', AuthService.getID());
        fetch(domain.getDomain() + ":8083/kollektiv/createKollektiv/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .then(console.log(AuthService.getID()))
            .catch(error => console.log(error));
        this.setState({message: "Du har nå laget et nytt kollektiv!"})
    }

    JoinKollektiv(event) {
        event.preventDefault();

        let data = {
            userid: AuthService.getID(),
            code: this.state.code
        };
        fetch(domain.getDomain() + ":8083/kollektiv/joinKol/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: "cors",
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .then(console.log(AuthService.getID()))
            .catch(error => console.log(error));
        this.setState({message: "Du har nå med i et Kollektiv"})
    }

    render() {
        if (isBrowser) {
            return (
                <div className="notInKollektivWrapper">
                    <div className="notInKollektivWindow">
                        <div className="createNewKollektivSide">
                            <h1>Lag et nytt kollektiv</h1>
                            <img src={hammer} alt="Lag nytt kollektiv"/>
                            <form onSubmit={this.createKollektiv}>
                                <p>Skriv inn navn på ditt nye kollektiv</p>
                                <input placeholder="Skriv inn navn" name="kollektivName" type="text" value={this.state.kollektivName} onChange={this.handleChange}/>
                                <br/>
                                <button className="FormField__Button__create__kollektiv">Lag nytt!</button>
                            </form>
                        </div>
                        <div className="joinKollektivSide">
                            <h1>Bli med i kollektiv</h1>
                            <img src={house} alt="Bli med i et eksisterende kollektiv"/>
                            <form onSubmit={this.JoinKollektiv}>
                                <p>Skriv inn invitasjonskode for kollektivet</p>
                                <input name="code" value={this.state.code} onChange={this.handleChange} placeholder="Skriv inn kode"/>
                                <br/>
                                <button className="FormField__Button__join__kollektiv">Bli med!</button>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
        else if (isMobile) {
            return (

                <router>
                        <div className="PageSwitcher__notInKollektiv_Mobile">
                            <NavLink exact to="/komponer"
                                     activeClassName="PageSwitcher__notInKollektiv__Item_mobile__Item--Active"
                                     className="PageSwitcher__notInKollektiv__Item_mobile">Lag nytt
                            </NavLink>
                            <NavLink exact to="/eksisterende"
                                     activeClassName="PageSwitcher__notInKollektiv__Item_mobile__Item--Active"
                                     className="PageSwitcher__notInKollektiv__Item_mobile">Bli med
                            </NavLink>
                        </div>
                        <div>
                            <Route path="/eksisterende" component={joinKollektivComponent}/>
                            <Route path="/komponer" component={createKollektivComponent}/>
                        </div>
                </router>

            );

        }
    }
}

export default notInKollektivPage;