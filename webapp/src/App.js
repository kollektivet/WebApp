import React, {Component} from 'react';
import {isBrowser, isMobile} from "react-device-detect";
import './App.css';
import AuthService from "./pages/AuthService";
import HomePage from "./pages/HomePage";
import loginpage from "./pages/LoginPage";
import NotInKollektivPage from "./pages/notInKollektivPage";
import KollektivService from "./pages/KollektivService";


class App extends Component {

    render() {

        //Checks if we are logged in and then returns the home page.
        if(AuthService.loggedIn()) {
            if (KollektivService.isMemberInKollektiv) {
                return (
                    <HomePage/>
                );
            }
            else {
                return (
                    <NotInKollektivPage/>
                );
            }
        }

        //If the user is not logged in, it runs the "else" part to return the login page.
        else{
            //Checks if user is using browser.
            if (isBrowser) {
                return (
                    loginpage.return_login_for_browser()
                );
            }
            //Checks if user is using a mobile device.
            else if (isMobile) {
                return (
                    loginpage.return_login_for_mobile()
                );
            }
        }
    }
}

export default App;